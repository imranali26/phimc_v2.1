<?php
require("../_data.php");
$id=$_GET["id"];
$a= array();
$i=0;
$sql= mysqli_query($conn,"SELECT * FROM `vw_hf` WHERE `tehsil` in (SELECT `id` FROM `tehsil` WHERE `district`=$id)");
while( $row =mysqli_fetch_array($sql)){
    $b=array();

    $b["id"]=$row["id"];
    $b["name"]=$row["name"];
    $b["lat"]=$row["lat"];
    $b["lng"]=$row["lng"];
    $b["tehsil"]=$row["tehsil"];
    $b["type"]=$row["type"];
    $b["cat_name"]=$row["cat_name"];
    $b["lic_name"]=$row["lic_name"];
    $b["beds"]=$row["beds"];
    $b["rec_for_emp"]=$row["rec_for_emp"];
    $b["pri_for_emp"]=$row["pri_for_emp"];
    $b["emp_status_name"]=$row["emp_status_name"];
    $b["key_features"]=$row["key_features"];
    $a[$i]=$b;
    $i++;
}
echo json_encode($a);
?>