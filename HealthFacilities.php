<?php
$title="Health Facilities";
require_once ("header.php");
?>

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Blank Page</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Page Layouts</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">
                            <i class="icon-bell"></i> Action</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-shield"></i> Another action</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-user"></i> Something else here</a>
                    </li>
                    <li class="divider"> </li>
                    <li>
                        <a href="#">
                            <i class="icon-bag"></i> Separated link</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Blank Page Layout
        <small>blank page layout</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
<div class="row">
        <div class="form-body">
            <div class="col-md-4">
                <div class="form-group form-md-line-input form-md-floating-label has-info">
                    <select class="form-control edited" id="dist">
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input form-md-floating-label has-info">
                    <select class="form-control edited" id="tehsil">
                        <option value="">Select Tehsil</option>
                    </select>
                </div>
            </div>

        </div>
        <button type="button" class="btn green" onclick="showHfByTehsil( $('#tehsil').val()); showMarkers()">Save changes</button>

</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN BASIC PORTLET-->

        <div  id="googleMap" style="width: 100%;
    height: 400px;
    float: left;
    margin: 0;
    padding: 0;">
        </div>

        <!-- END BASIC PORTLET-->
    </div>

</div>

<!-- Button trigger modal -->
<button class="btn btn-primary btn-lg" data-target="#full-width" data-toggle="modal"> View Demo </button>
<!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#centralModalInfo">-->
<!--    Launch demo modal-->
<!--</button>-->

<div id="full-width" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Full Width</h4>
    </div>
    <div class="modal-body">



<!--Body-->
<div class="row">
    <div class="col-md-12">
        <div class="note note-success">
            <p> Please try to re-size your browser window in order to see the tables in responsive mode. </p>
        </div>
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green">
<!--            <div class="portlet-title">-->
<!--                <div class="caption">-->
<!--                    <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables </div>-->
<!--                <div class="tools">-->
<!--                    <a href="javascript:;" class="collapse"> </a>-->
<!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
<!--                    <a href="javascript:;" class="reload"> </a>-->
<!--                    <a href="javascript:;" class="remove"> </a>-->
<!--                </div>-->
<!--            </div>-->
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content" id="tblhf">
                    <thead class="flip-content">
                    <tr>
                        <th width="10%"> Health Facility Name </th>
<!--                        <th width="20%"> Code </th>-->
                        <th> Type of HFs</th>
                        <th> Category of HFs</th>
                        <th> Licensing Status</th>
                        <th> No of Beds</th>
                        <th> Recommendation for Empanelment</th>
                        <th> Priority for Empanelment</th>
                        <th> Empanelment Status</th>
                        <th> Key Features</th>
<!--                        <th class="numeric"> Price </th>-->
                    </tr>
                    </thead >
                    <tbody id="dtdetails">
                    <tr>

                    </tr>
                    <tr>
<!--                        <td> AAD </td>-->
<!--                        <td> ARDENT LEISURE GROUP </td>-->
<!--                        <td class="numeric"> $1.15 </td>-->
<!--                        <td class="numeric"> +0.02 </td>-->
<!--                        <td class="numeric"> 1.32% </td>-->
<!--                        <td class="numeric"> $1.14 </td>-->
<!--                        <td class="numeric"> $1.15 </td>-->
<!--                        <td class="numeric"> $1.13 </td>-->
<!--                        <td class="numeric"> 56,431 </td>-->
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" >Save changes</button>
    </div>
</div>



<?php
require_once ("footer.php");
?>
<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->
<!--<script src="plugins/gmaps/gmaps.min.js" type="text/javascript"></script>-->
<!--<script src="scripts/maps-google.min.js" type="text/javascript"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFX2v5z34vYKl0We4nHV4KFV1j6uVsltg&callback=myMap" async defer></script>
<script type="text/javascript" src="scripts/myfunctions.js"></script>
<script type="text/javascript">

    retreiveDistrict();
    $('#dist').on('change',function() {
        var id = $('#dist').val();
        tehsilByDist(id);
    });
    var map;
    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(34, 72),
            zoom:5
        };
        map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }

//    var marker, i;
//
//    function showMarkers(){
//
//        var id = $('#tehsil').val();
//
//        if(id=="")
//            return;
//        var request= $.ajax({
//            url : 'api/hf/getByTehsil.php',
//            type : 'get',
//            data : {
//                id : id
//
//            }});
//        request.done(function(data ) {
//            console.log(data);
//            var res= JSON.parse(data);
//            $('#dtdetails td').remove();
//            var html = "";
//            for (i=0; i<res.length; i++){
//
//                marker = new google.maps.Marker({
//                    position: new google.maps.LatLng(res[i].lat, res[i].lng),
//                    offset: '0',
//                    title: res[i].name,
//                    map: map
//                });
//                addInfoWindow(marker, res[i].name);
//            }
//        });
//    }
//
//    function addInfoWindow(marker, message) {
//
//        var infoWindow = new google.maps.InfoWindow({
//            content: message
//        });
//
//        google.maps.event.addListener(marker, 'click', function () {
//            infoWindow.open(map, marker);
////            document.getElementById("hf_tb").innerHTML= message.hf_name;
////            document.getElementById("disct_tb").innerHTML= message.district;
////            document.getElementById("tehsil_tb").innerHTML= message.tehsil;
////            document.getElementById("cat_tb").innerHTML= message.cat_of_hf;
////            document.getElementById("type_tb").innerHTML= message.type_hf;
////            document.getElementById("beds_tb").innerHTML= message.beds;
////            document.getElementById("lic_tb").innerHTML= message.licensing_status;
////            document.getElementById("rec_tb").innerHTML= message.rec_for_emp;
////            document.getElementById("prior_tb").innerHTML= message.priority_for_emp;
////            document.getElementById("key_tb").innerHTML= message.key_features;
//        });
//    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFX2v5z34vYKl0We4nHV4KFV1j6uVsltg&callback=myMap" async defer></script>