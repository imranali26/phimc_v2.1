function retreiveDistrict() {
    var request= $.ajax({
        url : 'api/district/getByProvince.php',
        type : 'get',
        data : {
            id : 1

        }});
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        var html = "<option value=''>Select District</option>";
        for (i=0; i<res.length; i++)
        {
            html += '<option value='+res[i].id+'>'+res[i].name+'</option>';
        }
        $('#dist').append(html);
    });
}

function tehsilByDist(id) {
    var request= $.ajax({
        url : 'api/tehsil/getByDistrict.php',
        type : 'get',
        data : {
            id : id

        }});
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        $('#tehsil option').remove();
        var html = "<option value=''>Select Tehsil</option>";
        for (i=0; i<res.length; i++)
        {
            html += '<option value='+res[i].id+'>'+res[i].name+'</option>';
        }
        $('#tehsil').append(html);
    });
}

function showHfByTehsil(id,dist) {
    var target="";
    var paramId="";
    if(id==""){

    }
    var request= $.ajax({
        url : 'api/hf/getByTehsil.php',
        type : 'get',
        data : {
            id : id

        }});
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        $('#dtdetails td').remove();
        var html = "";
        for (i=0; i<res.length; i++)
        {
            html += '<tr >';

                html += '<td >'+res[i].name+'</td>';
                html += '<td >'+res[i].type+'</td>';
                html += '<td >'+res[i].cat_name+'</td>';
                html += '<td >'+res[i].lic_name+'</td>';
                html += '<td >'+res[i].beds+'</td>';
                html += '<td >'+res[i].rec_for_emp+'</td>';
                html += '<td >'+res[i].pri_for_emp+'</td>';
                html += '<td >'+res[i].emp_status_name+'</td>';
                html += '<td >'+res[i].key_features+'</td></tr>';

        }
        console.log(html);
        $('#dtdetails').append(html);
    });

}

function getCategories() {
    var request= $.ajax({
        url : 'api/category/get.php',
        type : 'get'
        });
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        var html = "<option value=''>Select Category</option>";
        for (i=0; i<res.length; i++)
        {
            html += '<option value='+res[i].id+'>'+res[i].name+'</option>';
        }
        $('#cat_of_hf').append(html);
    });
}

function getLicStatus() {
    var request= $.ajax({
        url : 'api/LicStatus/get.php',
        type : 'get'
    });
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        var html = "<option value=''>Select Category</option>";
        for (i=0; i<res.length; i++)
        {
            html += '<option value='+res[i].id+'>'+res[i].name+'</option>';
        }
        $('#lic_status').append(html);
    });
}

function getEmpStatus() {
    var request= $.ajax({
        url : 'api/EmpStatus/get.php',
        type : 'get'
    });
    request.done(function(data ) {
        console.log(data);
        var res= JSON.parse(data);
        var html = "<option value=''>Select</option>";
        for (i=0; i<res.length; i++)
        {
            html += '<option value='+res[i].id+'>'+res[i].name+'</option>';
        }
        $('#emp_status').append(html);
    });
}

// $("#submit_hf_data").click(function() {
//     var name = $("#name").val();
//     var dist = $("#dist").val();
//     var tehsil = $("#tehsil").val();
//     var lat = $("#lat").val();
//     var lng = $("#lng").val();
//     var type_of_hf = $("#type_of_hf").val();
//     var cat_of_hf = $("#cat_of_hf").val();
//     var lic_status = $("#lic_status").val();
//     var beds = $("#beds").val();
//     var rec_for_emp = $("#rec_for_emp").val();
//     var pri_for_emp = $("#pri_for_emp").val();
//     var emp_status = $("#emp_status").val();
//     var key_features = $("#key_features").val();
// });

function createHealthFacility(name,category) {
    var request= $.ajax({
        url : 'api/hf/create.php',
        type : 'post',
        data : {
            name : name



        }});
    var res= JSON.parse(data);


}

