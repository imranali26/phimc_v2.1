<?php
require_once ("header.php");
?>
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Blank Page</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Page Layouts</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>
                        <a href="#">
                            <i class="icon-bell"></i> Action</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-shield"></i> Another action</a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-user"></i> Something else here</a>
                    </li>
                    <li class="divider"> </li>
                    <li>
                        <a href="#">
                            <i class="icon-bag"></i> Separated link</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Blank Page Layout
        <small>blank page layout</small>
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN BASIC PORTLET-->


            <!-- END BASIC PORTLET-->
        </div>

    </div>


    <div class="row">
        <div class="col-md-offset-2 col-md-8 ">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-pin font-green"></i>
                        <span class="caption-subject bold uppercase">Health Facilities Database</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default dropdown-toggle" href="javascript:;" data-toggle="dropdown"> Settings
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;">
                                        <i class="fa fa-pencil"></i> Edit </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="fa fa-trash-o"></i> Delete </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="fa fa-ban"></i> Ban </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Make admin </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="name">
                                <label for="name">Health Facility Name</label>
                                <span class="help-block">Enter Health Facility Name...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="dist">
                                </select>
                                <label for="dist">District</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="tehsil">
                                    <option value="">Select Teshil</option>
                                </select>
                                <label for="tehsil">Tehsil</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="lat">
                                <label for="lat">Latitude of Health Facility</label>
                                <span class="help-block">Enter Latitude of Health Facility...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="lng">
                                <label for="lng">Longitude of Health Facility</label>
                                <span class="help-block">Enter Longitude of Health Facility...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="type_of_hf">
                                    <option value="Public">Public</option>
                                    <option value="Private">Private</option>
                                </select>
                                <label for="type_of_hf">Type of Health Facility</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="cat_of_hf">
                                    <option value=""></option>
                                </select>
                                <label for="cat_of_hf">Select Category of Health Facility</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="lic_status">

                                </select>
                                <label for="lic_status">Select Licensing Status of Health Facility</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="beds">
                                <label for="beds">No. of Beds</label>
                                <span class="help-block">Enter No. of Beds...</span>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="rec_for_emp">
                                    <option value=""></option>
                                </select>
                                <label for="rec_for_emp">Select Recommendation for Empanelment</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="pri_for_emp">
                                    <option value=""></option>

                                </select>
                                <label for="pri_for_emp">Select Priority for Empanelment</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label has-info">
                                <select class="form-control edited" id="emp_status">

                                </select>
                                <label for="emp_status">Select Empanelment Status</label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <input type="text" class="form-control" id="key_features">
                                <label for="key_features">Key Features</label>
                                <span class="help-block">Enter Key Features...</span>
                            </div>
                        </div>
                        <div class="form-actions noborder">
                            <button type="submit" class="btn blue" id="submit_hf_data">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->


        </div>
    </div>
<?php
require_once ("footer.php");
?>
<script type="text/javascript" src="scripts/myfunctions.js"></script>
<script type="text/javascript" >
    retreiveDistrict();
    $('#dist').on('change',function() {
        var id = $('#dist').val();
        tehsilByDist(id);
    });
    getCategories();
    getLicStatus();
    getEmpStatus();

</script>
